/* -*- coding: utf-8 -*- */
/**
\ingroup geom
@{
\file dpeuckera.c
\brief Declaración de funciones auxiliares para el uso de la familia de
       algoritmos de Douglas-Peucker.
\author José Luis García Pallero, jgpallero@gmail.com
\date 01 de abril de 2014
\copyright
Copyright (c) 2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"libgeoc/dpeuckera.h"
/******************************************************************************/
/******************************************************************************/
size_t* CasosEspecialesAligeraPolilinea(const double* x,
                                        const double* y,
                                        const size_t nPtos,
                                        const size_t incX,
                                        const size_t incY,
                                        const double tol,
                                        size_t* nPtosSal,
                                        int* hayCasoEspecial)
{
    //índice para recorrer bucles
    size_t i=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //vector de salida
    size_t* salida=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //en principio, el número de puntos de salida es el mismo que el de entrada
    *nPtosSal = nPtos;
    //inicializamos la variable indicadora de caso especial a 0
    *hayCasoEspecial = 0;
    //vamos comprobando casos especiales
    if(nPtos==0)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //actualizamos la variable de número de puntos de salida
        *nPtosSal = 0;
        //salimos de la función
        return NULL;
    }
    else if(nPtos==1)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //asignamos memoria para el vector de salida
        salida = (size_t*)malloc(sizeof(size_t));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //la entrada sólo es un punto
        *nPtosSal = 1;
        salida[0] = 0;
    }
    else if(nPtos==2)
    {
        //indicamos que estamos ante un caso especial
        *hayCasoEspecial = 1;
        //en principio, los dos puntos son válidos
        *nPtosSal = 2;
        //comprobamos si los puntos son o no el mismo
        if((x[0]==x[incX])&&(y[0]==y[incY]))
        {
            //sólo vale el primer punto
            *nPtosSal = 1;
        }
        //asignamos memoria para el vector de salida
        salida = (size_t*)malloc((*nPtosSal)*sizeof(size_t));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //copiamos los puntos válidos
        for(i=0;i<(*nPtosSal);i++)
        {
            salida[i] = i;
        }
    }
    else if((nPtos==3)&&(x[0]==x[2*incX])&&(y[0]==y[2*incY]))
    {
        //indicamos que estamos ante un caso especial
        *hayCasoEspecial = 1;
        //en principio, hay dos puntos válidos
        *nPtosSal = 2;
        //comprobamos también si el segundo punto es el mismo que el primero
        if((x[0]==x[incX])&&(y[0]==y[incY]))
        {
            //sólo vale el primer punto
            *nPtosSal = 1;
        }
        //asignamos memoria para el vector de salida
        salida = (size_t*)malloc((*nPtosSal)*sizeof(size_t));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //copiamos los puntos válidos
        for(i=0;i<(*nPtosSal);i++)
        {
            salida[i] = i;
        }
    }
    else if(atol==0.0)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //asignamos memoria para el vector de salida
        salida = (size_t*)malloc(nPtos*sizeof(size_t));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //inicializamos el número de puntos de salida y la posición del primero
        *nPtosSal = 1;
        salida[0] = 0;
        //recorremos el resto de puntos
        for(i=1;i<nPtos;i++)
        {
            //comprobamos si el punto de trabajo es distinto al anterior
            if((x[i*incX]!=x[(i-1)*incX])&&(y[i*incY]!=y[(i-1)*incY]))
            {
                //asignamos la posición del punto de trabajo
                salida[*nPtosSal] = i;
                //actualizamos el número de puntos de salida
                (*nPtosSal)++;
            }
        }
        //comprobamos si se ha quitado algún punto
        if(nPtos!=(*nPtosSal))
        {
            //reasignamos memoria para el vector de salida
            salida = (size_t*)realloc(salida,(*nPtosSal)*sizeof(size_t));
            //comprobamos si ha ocurrido algún error
            if(salida==NULL)
            {
                //mensaje de error
                GEOC_ERROR("Error de asignación de memoria");
                //salimos de la función
                return NULL;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return salida;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
