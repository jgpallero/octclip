/* -*- coding: utf-8 -*- */
/**
\ingroup gshhs geom proyec
@{
\file proyecaux.h
\brief Declaración de funciones de algunas proyecciones cartográficas para no
       usar PROJ.4.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de agosto de 2013
\copyright
Copyright (c) 2013, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _PROYECAUX_H_
#define _PROYECAUX_H_
/******************************************************************************/
/******************************************************************************/
#include<math.h>
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\brief Proyecta un punto según la proyección cilíndrica equivalente de Lambert,
       siendo el paralelo origen el ecuador.
\param[in] lat Latitud geodésica del punto de trabajo, en radianes.
\param[in] lon Longitud geodésica del punto de trabajo, en radianes.
\param[in] lon0 Longitud geodésica origen, en radianes.
\param[in] a Semieje mayor del elipsoide, en metros.
\param[in] f Aplanamiento del elipsoide.
\param[out] x Coordenada X proyectada.
\param[out] y Coordenada Y proyectada.
\note El incremento \em lon-lon0 no puede estar fuera del intervalo
      \f$[-\pi,\pi]\f$.
\date 22 de junio de 2011: Creación de la función.
\date 15 de noviembre de 2013: Adición de la capacidad de cálculo con puntos
      sobre la esfera.
*/
void ProjCilinEquivLambertLat0Ec(const double lat,
                                 const double lon,
                                 const double lon0,
                                 const double a,
                                 const double f,
                                 double* x,
                                 double* y);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
